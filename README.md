## Author
Luiz da Rocha-Schmidt (darocha@tum.de)

## Description
SetPPTLang is an Add-In for MS PowerPoint that allows to set the Language for entire PowerPoint presentations for use with automatic spelling correction. Currently German, Englisch (UK) and Englisch (US) are supported

## Download and Installation
[Download](https://gitlab.com/daLuiz/SetPPTLang/repository/archive.zip?ref=master) the zip file of the project or clone the repository using git. 
If you don't want the source code but only the Add-In, you only need the `/publish/` folder. There, double-click the `SetPPTLang.vsto` file to install the Add-In. Currently no valid code signing certificate is available, therefore certificate errors have to be ignored.

## How does it work?
The Add-In cycles through all slides. On each slide, it cycles through several objects, that typically make up presentation slides, such as: tables, text boxes, shapes. On each item, the language attribute is explicitly set to the selected language. This is done to a certain depth level, but not infinitely. If you have complex nested slides, some items might be missed. Feel free to open an issue in this case.

## How to contribute
SetPPTLang was created using Visual Studio 2015 Community and the Office Developer Tools, both of which are available for free online. Download and install them, now you can open and modify the `SetPPTLang.sln` file. The main code is in the `Ribbon1.vb`. In order to compile, you have to generate a temporary certificate in the Visual Studio Project options.