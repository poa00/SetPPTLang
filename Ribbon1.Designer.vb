﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Erforderlich für die Unterstützung des Windows.Forms-Klassenkompositions-Designers
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'Dieser Aufruf ist für den Komponenten-Designer erforderlich.
        InitializeComponent()

    End Sub

    'Die Komponente überschreibt den Löschvorgang zum Bereinigen der Komponentenliste.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Komponenten-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Komponenten-Designer erforderlich.
    'Das Bearbeiten ist mit dem Komponenten-Designer möglich.
    'Nehmen Sie keine Änderungen mit dem Code-Editor vor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Ribbon1))
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.Group1 = Me.Factory.CreateRibbonGroup
        Me.SetGerButton = Me.Factory.CreateRibbonButton
        Me.SetEngUKButton = Me.Factory.CreateRibbonButton
        Me.SetEngUSButton = Me.Factory.CreateRibbonButton
        Me.Tab1.SuspendLayout()
        Me.Group1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.Group1)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'Group1
        '
        Me.Group1.Items.Add(Me.SetGerButton)
        Me.Group1.Items.Add(Me.SetEngUKButton)
        Me.Group1.Items.Add(Me.SetEngUSButton)
        Me.Group1.Label = "Set Language"
        Me.Group1.Name = "Group1"
        '
        'SetGerButton
        '
        Me.SetGerButton.Image = CType(resources.GetObject("SetGerButton.Image"), System.Drawing.Image)
        Me.SetGerButton.Label = "Set German"
        Me.SetGerButton.Name = "SetGerButton"
        Me.SetGerButton.ScreenTip = "Sets the language of all text (including text boxes and tables) to German"
        Me.SetGerButton.ShowImage = True
        '
        'SetEngUKButton
        '
        Me.SetEngUKButton.Image = CType(resources.GetObject("SetEngUKButton.Image"), System.Drawing.Image)
        Me.SetEngUKButton.Label = "Set English (UK)"
        Me.SetEngUKButton.Name = "SetEngUKButton"
        Me.SetEngUKButton.ScreenTip = "Sets the language of all text (including text boxes and tables) to English (UK)"
        Me.SetEngUKButton.ShowImage = True
        '
        'SetEngUSButton
        '
        Me.SetEngUSButton.Image = CType(resources.GetObject("SetEngUSButton.Image"), System.Drawing.Image)
        Me.SetEngUSButton.Label = "Set English (US)"
        Me.SetEngUSButton.Name = "SetEngUSButton"
        Me.SetEngUSButton.ScreenTip = "Sets the language of all text (including text boxes and tables) to English (US)"
        Me.SetEngUSButton.ShowImage = True
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.PowerPoint.Presentation"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents Group1 As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents SetGerButton As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents SetEngUSButton As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents SetEngUKButton As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
