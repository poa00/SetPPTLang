﻿Imports Microsoft.Office.Tools.Ribbon

Public Class Ribbon1

    Private Sub Ribbon1_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load

    End Sub


    Private Sub SetLang(lang)
        Dim j As Integer, k As Integer, l As Integer, m As Integer, scount As Integer, fcount As Integer, colcount As Integer, celcount As Integer
        Dim ActivePresentation = Globals.ThisAddIn.Application.ActivePresentation
        ActivePresentation.DefaultLanguageID = lang
        scount = ActivePresentation.Slides.Count
        For j = 1 To scount
            fcount = ActivePresentation.Slides(j).Shapes.Count
            For k = 1 To fcount
                If ActivePresentation.Slides(j).Shapes(k).HasTable Then
                    colcount = ActivePresentation.Slides(j).Shapes(k).Table.Columns.Count
                    For l = 1 To colcount
                        celcount = ActivePresentation.Slides(j).Shapes(k).Table.Columns(l).Cells.Count
                        For m = 1 To celcount
                            ActivePresentation.Slides(j).Shapes(k).Table.Cell(m, l).Shape.TextFrame.TextRange.LanguageID = lang
                        Next m
                    Next l
                End If
                If ActivePresentation.Slides(j).Shapes(k).HasTextFrame Then
                    ActivePresentation.Slides(j).Shapes(k) _
                    .TextFrame.TextRange.LanguageID = lang

                End If
            Next k
        Next j
    End Sub


    Private Sub SetGerButton_Click(sender As Object, e As RibbonControlEventArgs) Handles SetGerButton.Click
        Dim lang As Microsoft.Office.Core.MsoLanguageID
        lang = Microsoft.Office.Core.MsoLanguageID.msoLanguageIDGerman
        SetLang(lang)
    End Sub

    Private Sub SetEngButton_Click(sender As Object, e As RibbonControlEventArgs) Handles SetEngUSButton.Click
        Dim lang As Microsoft.Office.Core.MsoLanguageID
        lang = Microsoft.Office.Core.MsoLanguageID.msoLanguageIDEnglishUS
        SetLang(lang)
    End Sub

    Private Sub SetEngUKButton_Click(sender As Object, e As RibbonControlEventArgs) Handles SetEngUKButton.Click
        Dim lang As Microsoft.Office.Core.MsoLanguageID
        lang = Microsoft.Office.Core.MsoLanguageID.msoLanguageIDEnglishUK
        SetLang(lang)
    End Sub
End Class
